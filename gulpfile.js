const { src, dest, parallel, watch, series } = require('gulp');
const connect = require('gulp-connect');
const sass = require('gulp-sass');
const concat = require('gulp-concat');

function server(cb) {
    connect.server({
        root: 'app',
        livereload: true
    })
    cb();
}

function build_scss(cb) {
    src('src/scss/styles.scss', {sourcemaps: true})
    .pipe(sass())
    .pipe(dest('app/css/'))
    .pipe(connect.reload());
    cb();
}

function build_html(cb) {
    src('src/index.html')
    .pipe(dest('app/'))
    .pipe(connect.reload());
    cb();
}

function build_js(cb) {
    src('src/js/**')
    .pipe(concat('app.js'))
    .pipe(dest('app/js/'));
    cb();
}

function copy_img(cb) {
    src('src/img/**')
    .pipe(dest('app/img/'));
}


watch('src/index.html', build_html);
watch('src/scss/*.scss', build_scss);
watch('src/js/**', build_js);

exports.default = series(server, build_scss, build_js, copy_img, build_html);
